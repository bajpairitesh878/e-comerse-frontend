import React from 'react';

const FooterHomeContent = () => {
    return (
        <>
            <div className='homefooter-content'>
                <div>
                    <h1>Best Place to Buy Refurbished Laptops in India</h1>
                    <p>A brand which deals in best refurbished laptops with lowest price in the market beating the second hand laptops in India and used laptops in India in terms of quality with high-performance and also providing best refurbished gaming laptops in India.</p>
                    <p>We are selling Refurbished laptops with warranty and trying to meet customer satisfaction which includes corporates and end users, checking the complete system with numbers of quality checks and delivering certified refurbished laptops in India is our first priority.</p>
                    
                    <hr />
                    <h4>Defining “Refurbished Laptop” term ? </h4>

                    <p>A common prejudice is seen among buyers about the refurbishment of laptops that they think refurbished laptops may be a bit different from the new ones or worse. This myth can be overlooked by choosing refurbished laptops. Keeping an eye on the economy of buying second hand laptops and selling used laptops has come into being. Among them, the most alluring idea to reuse and recycle laptops which are termed as Refurbished has a good reel in the business which are better than second hand laptops in India.</p>
                    <p>These laptops are being imported from various regions of world after their minimal usage, closing of projects or after upgradation in technology these laptops in bulk are auctioned as refurbished and thus imports-export of refurbish take place in market and with that Importer runs various quality test on the performance of laptops and after qualifying these laptops are available on website for sale for end users and for corporates.</p>

                    <hr />
                    <h4>What do you usually get from high quality refurbished laptops in India ?</h4>

                    <p>● 12 months warranty</p>

                    <p>● Lowest Price laptop in the entire market</p>

                    <p>● Free AMC services on demand</p>

                    <p>● Easy payment methods</p>

                    <p>● Checkup and visit at demand</p>

                    <p>● Remote support 24*7</p>

                    <hr />
                    <h4>REFURBISHED LAPTOP INDIA</h4>
                    <p>In today’s IT world, possessing a laptop becomes a necessity. Certified refurbished laptops in India come with many plus points. While shopping at lappyy.com, you can choose a variety of certified refurbished laptops in India from the best laptop brands available by checking features, comparing prices, and selecting the features most suitable for you and which are more reliable and trustworthy than useds laptops.</p>
                    <p>Lappyy.com has the best refurbished laptops prices that you can buy online. We sell premium certified refurbished laptops in India at insanely budget friendly prices. With the best quality which is better than second hand and used laptops in India. At lappyy.com, We assure you the best service possible as valuable customers, You can always contact us for any query.</p>
                    
                    <hr />

                    <h4>Why Buy from lappyy.com Store?</h4>
                    <p>The best thing about the brand is you only have to pay after seeing the product completely at your doorstep at a time of delivery in a selected region and we attempt to keep every process very transparent for customer satisfaction.</p>
                    <p>We try to interact directly with customers and understand their requirements and provide the best suitable Refurbished laptops.</p>
                    <p>These  products get inspected properly with all types of quality tests. Minor scratches or a bit wear and tear can be seen on the product, but the product are perfectly functional and We never take any payment before showing the product to customers so purchase refurbished laptops without any hesitation </p>
                    <hr />
                    <h4>Buy refurbished laptops online</h4>
                    <p>High Performance laptops designed for working and business professionals to satisfy their needs. Best refurbished online store in India with a good service warranty on refurbished laptops that you simply can purchase online.</p>
                    <p>refurbished laptops buy online india | refurbished gaming laptops india | second hand laptop online india | best refurbished online store india | used laptops india | dell refurbished laptops india | hp refurbished laptops india | lenovo refurbished laptops india | apple refurbished laptops india | refurbished macbook india | macbook pro refurbished india | macbook refurbished india | refurbished apple laptop</p>
                    
                    <hr /></div>

            </div>
        </>
    )
}

export default FooterHomeContent