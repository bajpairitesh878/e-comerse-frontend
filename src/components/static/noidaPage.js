import React from 'react';
import Banner from '../common/banner';
import NewarrivalSlider from '../common/arrivel';
import NewarrivalSlider2 from '../common/arrivel2';
import Footer from '../common/footer';
import axios from 'axios';
import ProductCard from '../common/product-card';
import { API_URL } from './../../services/index';
import MediaQuery from 'react-responsive';
import HeaderComponent from '../common/header.component';
import MobileHeader from '../common/headermobile.component';
import './../../assest/css/mobile.css';
import CategoryApple from './../../assest/img/top-category/apple.png';
import CategoryLenovo from './../../assest/img/top-category/lenovo.png';
import CategoryDell from './../../assest/img/top-category/dell.jpeg';
import CategoryHP from './../../assest/img/top-category/hp.png';
import CategoryLast from './../../assest/img/top-category/last.jpeg';
import ReactHtmlParser from 'react-html-parser';
import MetaTags from 'react-meta-tags';

export default class Home extends React.Component {
    constructor(props) {
        super();
        this.state = {
            'card_images': [],
            'featured_product': [],
            'recent_product': [],
            'bestshell_product': [],
            'testimonials': [],
            'product_enquery': '',
            'footerContent': [],
            'list_slider': [],
        }
    }

    async getHomeProduct() {
        await axios.get(API_URL + 'home/products')
            .then(response => {
                this.setState({
                    'featured_product': response.data['feature_product'],
                    'recent_product': response.data['recent_product'],
                    'bestshell_product': response.data['best_seller']
                })
            })
    }

    async getFooterContent() {
        axios.post(API_URL + 'pages/home/footer', {
            'page_address': 'before_footer',
            'page_name': 'noida'
        })
            .then(res => {
                this.setState({
                    footerContent: res.data
                })
            })
            .catch(err => console.log(err))
    }

    componentDidMount() {
        this.getHomeProduct();
        this.getFooterContent();
    }
    render() {
        return (
            <React.Fragment>
                <MetaTags>
                    <title>Refurbished Laptops Noida | Second Hand Laptop | Upto 60%OFF | Lappyy</title>
                    <meta name="description" content='Top deals on refurbished laptops in Noida @ cheapest price in the market beating the second hand laptops and used laptops in quality & performance with 1 Year Warranty. Pay at your doorstep in Delhi NCR ' />
                    <meta property="og:title" content="Refurbished Laptops Noida | Second Hand Laptop | Upto 60%OFF | Lappyy" />
                </MetaTags>
                <div>
                    <MediaQuery maxWidth={1224}>
                        <MobileHeader />
                    </MediaQuery>
                    <MediaQuery minWidth={1224}>
                        <HeaderComponent />
                    </MediaQuery>

                    {/* Main Slider Start */}
                    <div className="header">
                        <div className="container-fluids">
                            <div className="rows">
                                {/* <div className="col-md-12"> */}
                                <div>
                                    <div className="header-slider normal-slider">
                                        <Banner />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />

                    {/* Feature End*/}
                    {/* Category Start*/}
                    <div className="category product">
                        <div className="container-fluid">
                            <div className="section-header">
                                <h3>Top Category</h3>
                            </div>
                            <div className="row">
                                <div className="col-md-8">
                                    <div style={{ float: 'left', paddingLeft: '0px' }} className="col-md-6 mobile-card">
                                        <div className="category-item ch-400">
                                            <img src={CategoryApple} />
                                        </div>
                                    </div>
                                    <div style={{ float: 'left', paddingLeft: '0px' }} className="col-md-6 mobile-card">
                                        <div className="category-item ch-400">
                                            <img src={CategoryLenovo} />
                                        </div>
                                    </div>
                                    <div style={{ float: 'left', paddingLeft: '0px' }} className="col-md-6 mobile-card">
                                        <div className="category-item ch-400">
                                            <img src={CategoryDell} />
                                        </div>
                                    </div>
                                    <div style={{ float: 'left', paddingLeft: '0px' }} className="col-md-6 mobile-card">
                                        <div className="category-item ch-400">
                                            <img src={CategoryHP} />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div style={{ float: 'left', height: '100%' }}>
                                        <div className="category-lastslider">
                                            <img className='category-lastslider-img' src={CategoryLast} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Category End*/}

                    {/* Featured Product Start */}
                    <div className="featured-product product">
                        <div className="container-fluid">
                            <div className="section-header">
                                <h3>FEATURED PRODUCT</h3>
                            </div>
                            <div className="row align-items-center product-slider product-slider-4">
                                {this.state.featured_product && this.state.featured_product.slice(0, 4).map(product => {
                                    return (
                                        <div className="col-lg-3">
                                            <ProductCard product={product} />
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                    <MediaQuery minWidth={1224}>
                        <NewarrivalSlider data={this.state.list_slider} />
                    </MediaQuery>

                    {/* Featured Product End */}

                    {/* Recent Product Start */}
                    <div className="recent-product product">
                        <div className="container-fluid">
                            <div className="section-header">
                                <h3>Recent Product</h3>
                            </div>
                            <div className="row align-items-center product-slider product-slider-4">
                                {this.state.recent_product && this.state.recent_product.slice(0, 8).map(product => {
                                    return (
                                        <div className='col-lg-3'>
                                            <ProductCard product={product} />
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                    {/* Recent Product End */}
                    <MediaQuery minWidth={1224}>
                        <NewarrivalSlider2 data={this.state.list_slider} />
                    </MediaQuery>

                    {/* Newsletter End */}
                    {/* Recent Product Start */}
                    <MediaQuery minWidth={1224}>
                        <div className="recent-product product">
                            <div className="container-fluid">
                                <div className="section-header">
                                    <h3>Best Seller</h3>
                                </div>
                                <div className="row align-items-center product-slider product-slider-4">
                                    {this.state.bestshell_product && this.state.bestshell_product.slice(0, 8).map(product => {
                                        return (
                                            <div className="col-lg-3">
                                                <ProductCard product={product} />
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    </MediaQuery>
                    {/* Recent Product End */}
                    <div className='container product'>
                        <div className="section-header">
                            <h3>The Refurbished Hub</h3>
                        </div>
                        <div className='row refurb-content homefooter-content'>
                        {ReactHtmlParser(this.state.footerContent.content)}
                        </div>
                    </div>

                    <Footer />
                </div>

            </React.Fragment>
        )
    }
}