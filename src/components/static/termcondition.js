import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { API_URL } from './../../services/index';
import MediaQuery from 'react-responsive';
import HeaderComponent from '../common/header.component';
import MobileHeader from '../common/headermobile.component';
import Footer from '../common/footer';
import ReactHtmlParser from 'react-html-parser';
import MetaTags from 'react-meta-tags';

const TermCondition = () => {
    const [content, setContent] = useState('')
    useEffect(() => {
        TermContent()
    }, [])

    const TermContent = () => {
        axios.post(API_URL + 'pages/cms', {
            'page_name': 'term_conditions'
        })
            .then(res => {
                console.log(res.data.content)
                setContent(res.data)
            })
            .catch(err => console.log(err))
    }

    return (
        <>
            <MetaTags>
                <title>Term & Condition</title>
                <meta name="description" content='Term & Condition' />
            </MetaTags>
            <MediaQuery maxWidth={1224}>
                <MobileHeader />
            </MediaQuery>
            <MediaQuery minWidth={1224}>
                <HeaderComponent />
            </MediaQuery>
            <div className='aboutus-main'>
                <div className='top-containt'>
                    <h1>Term & Condition</h1>
                </div>
                <div className='content container'>
                    <div className='row refurb-content homefooter-content'>
                        {/* <FooterHomeContent /> */}
                        {ReactHtmlParser(content.content)}
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default TermCondition