import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import './../../assest/css/mobile.css';
import axios from 'axios';
import { BASH_URL, API_URL } from './../../services/index';
import { Modal } from 'react-responsive-modal';

const MobileHeader = () => {
  const [logo, setLogo] = useState('');
  const [category, setCategory] = useState('');
  const [price, setPrice] = useState('');
  const [menu, setMenu] = useState(false);
  const [open, setOpen] = useState(false);
  const [searchResult, setSearchResult] = useState('');

  useEffect(() => {
    Setting()
    getPricefilter()
    Category()
  }, [])

  const getPricefilter = () => {
    axios.get(`${API_URL}filter/price`)
      .then(response => {
        setPrice(response.data)
      })
      .catch(error => console.log(error))
  }

  const Category = () => {
    axios.get(API_URL + 'home/header/category')
      .then(response => {
        setCategory(response.data)
        console.log(response.data)
      })
      .catch(error => console.log(error))
  }

  const Setting = () => {
    axios.get(API_URL + 'app/settings')
      .then(response => {
        setLogo(response.data[0].site_logo)
        console.log(response.data[0].site_logo)
      })
      .catch(error => console.log('Setting', error))
  }

  const conrollCategory = () => {
    if (menu) {
      setMenu(false)
    } else {
      setMenu(true)
    }
  }

  const onCloseModal = () => {
    setOpen(false)
  }

  const openModel = () => {
    setOpen(true)
  }

  const searchResponse = (event) => {
    axios.get(API_URL + `header/search?search=${event.target.value}`)
      .then(response => {
        setSearchResult(response.data)
      })
      .catch(error => console.log(error))
  }

  return (
    <>
      {!menu &&
        <div className='mobile-logo'>
          <a href="/" className='mobile-link'>
            <img src={`${BASH_URL}${logo}`} alt="Logo" />
          </a>
        </div>
      }
      {menu &&
        <>
          <div className='category-view'>
            <div className='category-list'>
              <div className='row'>
                {category && category.map(item => {
                  return (
                    <>
                      {item.category_name == "Brand" &&
                        <>
                          {item.sub_category && item.sub_category.map(cat => {
                            return (
                              <>
                                <div className='mobile-subcategory'>
                                  <ul>
                                    <li>
                                      <a href={'/category/' + cat.slug}>
                                        <img src={BASH_URL + cat.category_image} />
                                        <p>{cat.category_names}</p>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </>
                            )
                          })}
                        </>
                      }
                    </>
                  )
                }
                )}
              </div>
            </div>
            <hr></hr>
            <div className='price-filter'>
              <ul style={{ padding: '0px' }}>
                {price && price.map(item => {
                  return (
                    <>
                      <li>₹{item.price_minimum}-₹{item.price_maximum}</li>
                    </>
                  )
                })}
              </ul>
            </div>
          </div>
        </>
      }
      <div className='mobile-header'>
        <div className='mobile-nav'>
          <ul className='mobile-list'>
            <li><i className='fa fa-home'></i><br />Home</li>
            <li onClick={() => conrollCategory()}><i className="fas fa-align-justify"></i><br />Category</li>
            <li onClick={()=>openModel()}><i className="fas fa-search"></i><br />Search</li>
            <li><i className="far fa-user-circle"></i><br />Account</li>
          </ul>
        </div>
      </div>
      <Modal open={open} onClose={onCloseModal}>
        <input className='modal-search' type="text" placeholder="Search" onChange={(event) => searchResponse(event)} />
        {/* <button className='modal-search-button'><i className="fa fa-search" /></button> */}
        {/* Search Listing */}
        {searchResult && searchResult.map(result => {
          return (
            <ul className="product-search">
              <li className="list-product"><a href={`/product/${result.slug}`}>
                <div className='product-image-search'>
                  <img src={`${result.small_image}`} />
                </div>
                <div className='product-content'>
                  <p>{result.product_name}</p>
                  {/* <hr />
                                                        <p>{ReactHtmlParser(result.product_description.slice(0, 50))}</p> */}
                  <hr />
                  <p><del>₹ {result.product_price_actual}</del> <span>₹ {result.product_price_offer}</span></p>
                </div>
              </a>
              </li>
            </ul>
          )
        })}
      </Modal>

    </>
  )
}

export default MobileHeader;