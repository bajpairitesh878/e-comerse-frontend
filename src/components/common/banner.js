import React from 'react';
import Slider from "react-slick";
import axios from "axios";
import { BASH_URL, API_URL } from './../../services/index';
import Banner1 from './../../assest/img/banner/banner-slider-1.jpg';
import Banner2 from './../../assest/img/banner/banner-slider-2.jpg';
import Banner3 from './../../assest/img/banner/banner-slider-3.jpg';


export default class Banner extends React.Component {
    state = {
        'list_banners': [],
        open: false,
    }
    async getBanner() {
        axios.get(API_URL + 'home/banner/top')
            .then(response => {
                this.setState({
                    'list_banners': response.data
                })
            })
            .catch(error => console.log(error))
    }

    onCloseModal = () => {
        this.setState({ open: false });
    };

    onOpenModal(product) {
        this.setState({
            open: true,
        });
    };

    componentDidMount() {
        // this.getBanner();
    }
    render() {
        const { open } = this.state;
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000

        }
        return (
            <div>

                <Slider {...settings}>
                    <div className="header-slider-item">
                        <img src={Banner1}  alt="Slider Image" className='slider-image' />
                    </div>
                    <div className="header-slider-item">
                        <img src={Banner2} alt="Slider Image" className='slider-image' />
                    </div>
                    <div className="header-slider-item">
                        <img src={Banner3} alt="Slider Image" className='slider-image' />
                    </div>
                    {/* {this.state.list_banners.map(banner => {
                        return (
                            <div className="header-slider-item">
                                <img src={`${BASH_URL}` + banner.slider_banner} alt="Slider Image" className='slider-image' />
                            </div>
                        )
                    })} */}

                </Slider>
                {/* <div className='header-search-main'>
                    <Modal className='enquiry-model' open={open} onClose={this.onCloseModal}>
                        <EnquiryForm />
                    </Modal>
                </div> */}
            </div>
        );
    }
}