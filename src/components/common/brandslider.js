import React from 'react';
import Slider from "react-slick";

export default class BrandSlider extends React.Component {
    render() {
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000
        };
        return (
            <div>
                <Slider {...settings}>
                    <div className="container-fluid">
                        <div className="brand-slider">
                            <div className="brand-item"><img src="../../assest/img/brand-1.png" alt="" /></div>
                        </div>
                    </div>
                    <div className="container-fluid">
                        <div className="brand-slider">
                            <div className="brand-item"><img src="../../assest/img/brand-1.png" alt="" /></div>
                            <div className="brand-item"><img src="../../assest/img/brand-2.png" alt="" /></div>
                            <div className="brand-item"><img src="../../assest/img/brand-3.png" alt="" /></div>
                            <div className="brand-item"><img src="../../assest/img/brand-4.png" alt="" /></div>
                            <div className="brand-item"><img src="../../assest/img/brand-5.png" alt="" /></div>
                            <div className="brand-item"><img src="../../assest/img/brand-6.png" alt="" /></div>
                        </div>
                    </div>
                </Slider>
            </div>
        )
    }
}