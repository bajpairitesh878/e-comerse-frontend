import React from 'react';
import Routes from './routes/index';
import './assest/css/style.css'

function App() {
  return (
    <div className="App">
      <Routes />
    </div>
  );
}

export default App;
