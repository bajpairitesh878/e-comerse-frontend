import React from 'react';
import Slider from "react-slick";
import axios from "axios";
import EnquiryForm from '../common/enquiryform';
import { Modal } from 'react-responsive-modal';
import { BASH_URL, API_URL } from './../../services/index';

export default class Banner extends React.Component {
    state = {
        'list_banners': [],
        open: false,
    }
    async getBanner() {
        axios.get(API_URL + 'home/banner/top')
            .then(response => {
                this.setState({
                    'list_banners': response.data
                })
            })
            .catch(error => console.log(error))
    }

    onCloseModal = () => {
        this.setState({ open: false });
    };

    onOpenModal(product) {
        this.setState({
            open: true,
        });
    };

    componentDidMount() {
        this.getBanner();
    }
    render() {
        const { open } = this.state;
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000

        }
        return (
            <div>

                <Slider {...settings}>
                    {this.state.list_banners.map(banner => {
                        return (
                            <div>
                                <div className="header-slider-item">
                                    <img src={`${BASH_URL}` + banner.slider_banner} alt="Slider Image" className='slider-image' />
                                    <div className="header-slider-caption">
                                        <p style={{ color: banner.banner_heading_color }}>{banner.banner_heading}</p>
                                        <a className="btn" onClick={() => this.onOpenModal()}><i className="fa fa-shopping-cart" />Enquire Now</a>
                                    </div>
                                </div>
                            </div>
                        )
                    })}

                </Slider>
                <div className='header-search-main'>
                    <Modal className='enquiry-model' open={open} onClose={this.onCloseModal}>
                        <EnquiryForm />
                    </Modal>
                </div>
            </div>
        );
    }
}